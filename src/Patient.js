import React, {useState, useEffect} from "react";
import uuid from 'react-uuid';

const Patient=(props)=>{
    const [attributes, setAttributes] = useState([]);
    const [instance, setInstance] = useState({});
    useEffect(()=>{
        fetch("http://www.projectware.net:8890/sparql?default-graph-uri=urn%3Asparql%3Abind%3Aekseli-designer-tka-100&query=select+%3Fproperty%2C+%3Flabel%2C+%3Ftype+%7B%0D%0A%3Fproperty+%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23domain%3E+%3Chttp%3A%2F%2Fwww.ekseli.fi%2F"+props.class+"%3E.%0D%0A%3Fproperty+a+%3Chttp%3A%2F%2Fwww.w3.org%2F2002%2F07%2Fowl%23DatatypeProperty%3E.%0D%0A%3Fproperty+%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23range%3E+%3Ftype.%0D%0Aoptional+%7B%0D%0A+++++%3Fproperty+rdfs%3Alabel+%3Flabel%0D%0A+++++filter+langMatches%28lang%28%3Flabel%29%2C+%22fi%22%29%0D%0A+++%7D%0D%0A+++optional+%7B%0D%0A+++++%3Fproperty+rdfs%3Alabel+%3Flabel%0D%0A+++%7D%0D%0A%7D&should-sponge=&format=application%2Fsparql-results%2Bjson&timeout=0&debug=on&run=+Run+Query+")
        .then(response=>response.json())
        .then(data=>{
            setAttributes(data.results.bindings)
        })
    },[])

    function update(event){
        setInstance({...instance, [event.target.id]:event.target.value});
    }
    
    function updateSparql(){
        //loop instance and if the field has value save it to graphql backend
        const uid =uuid();
        for (const [key, value] of Object.entries(instance)){   
            if(value!=undefined&&value!==null){
                const insertQuery = 'INSERT {' + 
                'eks:'+uid+' rdf:type owl:NamedIndividual, ' +
                'eks:' + props.class +';'+
                'eks:' + key + ' "'+ value+ '"^^xsd:string ;' +
                    'rdfs:label "'+uid+'" .}'; 
                console.log(insertQuery);
                const url = "http://projectware.net:8890/sparql?query="+encodeURIComponent(insertQuery) + '&default-graph-uri=' + encodeURIComponent("urn:sparql:bind:ekseli-designer-tka-99");
                console.log(url);
                fetch(url)
                .then(response=>{
                    document.getElementById("result").innerHTML = '<div><a target="_blank" href="http://www.projectware.net:8890/sparql?default-graph-uri=urn%3Asparql%3Abind%3Aekseli-designer-tka-99&query=select+*+%7B%3Fs+%3Fp+%3Fo%7D&should-sponge=&format=text%2Fhtml&timeout=0&debug=on&run=+Run+Query+">Katso graphin sisältö</a></div>'
                });
            }
        }
    }

    const attributeDiv= attributes.map((attribute, indes)=>{
        return <div id={attribute.property.value}><label for={attribute.property.value}>{attribute.label.value}</label><input id={attribute.label.value} value={instance[attribute.label.value]} onBlur={(event)=>update(event)} type="text"/></div>
    })

    return(
        <div>
            <h2>Add {props.class}</h2>
            {attributeDiv}<br/>
            {JSON.stringify(instance)}<br/>
            <button onClick={updateSparql}>Save {props.class}</button>
            <div id="result"></div>
            <br/>
            <br/>
            <div><a target="_blank" href="http://www.projectware.net:8890/sparql?default-graph-uri=urn%3Asparql%3Abind%3Aekseli-designer-tka-99&query=clear+graph+%3Curn%3Asparql%3Abind%3Aekseli-designer-tka-99%3E&should-sponge=&format=text%2Fhtml&timeout=0&debug=on&run=+Run+Query+">!!!!!Tyhjennä graafi!!!!!</a></div>
        </div>
    )
}

export default Patient;